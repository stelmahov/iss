// main map layer with leaflet
var map = L.map('map').setView([0,0], 2);

// getting coordinates with jQuery from API Open-Notify
function moveISS () {
    $.getJSON('http://api.open-notify.org/iss-now.json?callback=?', function(data) {
        var lat = data['iss_position']['latitude'];
        var lon = data['iss_position']['longitude'];
        // putting them to divs getting element by ID
        var latitude = document.getElementById('span-latitude');
        var longitude = document.getElementById('span-longitude');
        latitude.innerHTML = lat;
        longitude.innerHTML = lon;

        iss.setLatLng([lat, lon]);
        isscirc.setLatLng([lat, lon]);
        map.panTo([lat, lon], animate=true);
    });
    // updating 5 seconds
    setTimeout(moveISS, 5000); 
}

// initializing map layer
L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    maxZoom: 6,
}).addTo(map);

// add ISS icon and shadow 
var ISSIcon = L.icon({
    iconUrl: 'images/ISSIcon.png',
    iconSize: [50, 30],
    iconAnchor: [25, 15],
    popupAnchor: [50, 25],
    shadowUrl: 'images/ISSIcon_shadow.png',
    shadowSize: [60, 40],
    shadowAnchor: [30, 15]
});


var iss = L.marker([0, 0], {icon: ISSIcon}).addTo(map);
var isscirc = L.circle([0,0], 2200e3, {color: "#c22", opacity: 0.3, weight:1, fillColor: "#c22", fillOpacity: 0.1}).addTo(map); 

moveISS();


// getting number of People in Space and their names
$.getJSON('http://api.open-notify.org/astros.json?callback=?', function(data) {
    var number = data['number'];
    $('#numb').html(number);

    data['people'].forEach(function (d) {
        // add li tag with astronaut name with bootstrap class list-group-item to ul
         $('#people').append('<li class="list-group-item">' + '<img src="images/user.svg"/>' + d['name'] + '</li>');
    });
});

// get the date from new Date object
function currentUTCTime () {
    var date = new Date();
    // getting hours and minutes in our format with get method
    var hours = date.getHours();
    // add 0 in the beginning not to show something like 7:11
    if(hours < 10){
        hours = '0' + hours;    
    }
    var minutes = date.getMinutes();
    // add 0 in the beginning not to show something like 17:1
    if(minutes < 10){
        minutes = '0' + minutes;    
    }
    // add to the document with getting it by querySelector
    var time = document.querySelector('#currentTime');
    time.innerHTML = (hours + ":" + minutes);
    // updating 1 second
    setTimeout(currentUTCTime, 1000)
}
currentUTCTime();

// get the date from new Date object
function currentDate () {
    var date = new Date();
    // number of day in week
    var day = date.getDay();
    // use getWeekDay to transform "0" to "Sunday" 
    var currentDay = getWeekDay(day);
    // get day number in month 1-31
    var dayNumInMonth = date.getDate();
    // get month number
    var month = date.getMonth();
    // use getMonthName to transform "0" to "January" 
    var currentMonth = getMonthName(month);
    // get year
    var year = date.getFullYear();
    var today = document.querySelector('#currentDate');
    // put it in self format to the html
    today.innerHTML = (currentDay + ', ' + dayNumInMonth + ' ' + currentMonth + ' ' + year);
    // updating 50 sec
    setTimeout(currentDate, 50000);
}
currentDate();

// transforming 0,1,2,3 to 'Sunday', 'Monday', 'Tuesday', 'Wednesday'...
function getWeekDay (weekDayNumber){
    var weekDayNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', "Saturday"];
    return weekDayNames[weekDayNumber];
}

// transforming 0,1,2,3 to 'Jan', 'Feb', 'Mar', 'Apr'...
function getMonthName (monthNumber){
    var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov'];
    return monthNames[monthNumber];
}